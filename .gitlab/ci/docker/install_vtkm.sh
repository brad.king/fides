#!/bin/sh

cd /tmp
mkdir -p Software

# Install VTK-m
cd /tmp/Software
git clone -n https://gitlab.kitware.com/vtk/vtk-m.git
cd vtk-m
git checkout $VTKM_HASH_ENV
cd ..
mkdir -p vtkm-build
cd vtkm-build
if [ "$1" = "asan" ]; then
  cmake -GNinja \
    -DCMAKE_INSTALL_PREFIX=/opt/vtkm \
    -DBUILD_SHARED_LIBS=ON \
    -DCMAKE_BUILD_TYPE=Debug \
    -DCMAKE_CXX_FLAGS_DEBUG='-g -fsanitize=address -fsanitize=undefined' \
    -DVTKm_ENABLE_TESTING=OFF \
    -DVTKm_USE_DOUBLE_PRECISION=ON \
    ../vtk-m
else
  cmake -GNinja \
    -DCMAKE_INSTALL_PREFIX=/opt/vtkm \
    -DBUILD_SHARED_LIBS=ON \
    -DCMAKE_BUILD_TYPE=Debug \
    -DVTKm_ENABLE_TESTING=OFF \
    -DVTKm_USE_DOUBLE_PRECISION=ON \
    ../vtk-m
fi
ninja
ninja install

cd /tmp
rm -fr Software/
