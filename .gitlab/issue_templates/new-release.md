<!--
This template is for tracking a release of Fides. Please replace the
following strings with the associated values:

  - `@VERSION@` - replace with base version, e.g., 5.7.0
  - `@MAJOR@` - replace with major version number
  - `@MINOR@` - replace with minor version number

Please remove this comment.
-->

# High level view of Fides release process
1. Craft Release Branch
    - Generate change log
    - Commit change log
    - Commit update to version.txt
    - Merge release branch
2. Tag release
3. Add Gitlab Release Notes


# Craft Release Branch

## Generate change log

  - [ ] Update `master` branch for **fides**
```
git fetch origin
git checkout master
git merge --ff-only origin/master
```
  - [ ] Create release branch
```
git checkout -b release_@VERSION@
```
  - [ ] Make `docs/dev/changelog/@VERSION@/` directory for release notes
  - [ ] Create `docs/dev/changelog/@VERSION@/release-notes.md` file.
    - For each individual file in `docs/dev/changelog` move them to the relevant `release-notes` section.
      - Make sure each title and entry DOESNT have a period at the end
      - Make sure any sub-heading as part of the changelog is transformed from `##` to `###`.
    - Use the following template in `release-notes.md`
```md
Fides N Release Notes
=======================

# Table of Contents
1. [Core](#Core)
    - Core change 1
2. [Build](#Build)
3. [Other](#Other)


# Core

## Core change 1 ##

changes in core 1

# Build


# Other
```


## Commit change log

  - [ ] Remove each individual change log file from the git repository
```
git rm docs/dev/changelog/*.md
```
  - [ ] Add the changelog to git
```
git add docs/dev/changelog/@VERSION@/release-notes.md
```
  - [ ] Make a commit with the message:
```
Add release notes for v@VERSION@
```


## Commit update to version.txt

  - [ ] Update `version.txt` and commit. The commit message should read:
```
@VERSION@ is our Nth official release of Fides.

The major changes to Fides can be found in:
  docs/dev/changelog/@VERSION@/release-notes.md
```


## Merge release branch

  - [ ] Push the `release_@VERSION@` branch to gitlab and open a merge request.
    - Before completing the merge it is a good idea to review the rendered markdown of the release notes to ensure that they display properly.
    On the merge request page, click on the Changes tab and then for the `release.@VERSION@` file click the View file button in the upper right.


# Tag Release

Once the merge request is merged we can add the tag.
Fides marks the commit that contains the modifications to `version.txt` as the tag location, not the merge commit.
After the merge this would be the second commit shown by git log as shown below:

```
git checkout master
git pull
git log -n2
  Merge: d3d3e441 f66d980d
  commit f66d980d (HEAD -> release_@VERSION@, gitlab/release_@VERSION@, master)
```

  - [ ] Tag release using the following instructions:
```
git tag -a -f v@VERSION@ SHA1
```

For the above example the `SHA1` would be `f66d980d`

This should prompt you to add a message to the tag. The message should be identical to the one
you used in the commit.

  - [ ] Push the tag back to gitlab:
```
git remote add origin_update_tags git@gitlab.kitware.com:vtk/fides.git
git push --tags origin_update_tags
git remote rm origin_update_tags
```

# Add Gitlab Release Notes

Now that the Fides release is on gitlab we have to add the associated changelog to the release entry on gitlab.

  - [ ] Go to `https://gitlab.kitware.com/vtk/fides/-/tags/v@VERSION@/release/edit` to edit the release page.
    - Copy the contents of `docs/dev/changelog/@VERSION@/release-notes.md`


/cc @caitlin.ross
