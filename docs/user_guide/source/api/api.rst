###
API
###

.. role:: cpp(code)
   :language: c++
   :class: highlight

********
User API
********

DataSetReader class
-------------------

.. doxygenclass:: fides::io::DataSetReader
   :members:

Keys and MetaData
-----------------

.. doxygenfunction:: fides::keys::NUMBER_OF_BLOCKS

.. doxygenfunction:: fides::keys::NUMBER_OF_STEPS

.. doxygenfunction:: fides::keys::BLOCK_SELECTION

.. doxygenfunction:: fides::keys::FIELDS

.. doxygenfunction:: fides::keys::STEP_SELECTION

.. doxygenfunction:: fides::keys::PLANE_SELECTION


.. doxygenstruct:: fides::metadata::Size
   :members:
   :undoc-members:


.. doxygenstruct:: fides::metadata::Index
   :members:
   :undoc-members:


.. doxygenstruct:: fides::metadata::FieldInformation
   :members:
   :undoc-members:


.. doxygenstruct:: fides::metadata::Vector
   :members:
   :undoc-members:


.. doxygenstruct:: fides::metadata::Set
   :members:
   :undoc-members:


.. doxygenclass:: fides::metadata::MetaData
   :members:


Useful enums and typedefs
-------------------------

.. doxygenenum:: fides::StepStatus

.. doxygentypedef:: fides::DataSourceParams

.. doxygentypedef:: fides::Params
